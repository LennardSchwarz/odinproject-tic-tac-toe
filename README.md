# Tic-Tac-Toe

#### [Check it out](https://lennardschwarz.gitlab.io/odinproject-tic-tac-toe/)

### Play against a friend or try beating the unbeatable AI.

### Tools used: 

* HTML
* CSS (GRID) :black_square_button:
* Vanilla JS
 
Project has been completed while following The Odin Project's curriculum.

The implementation of the minimax algorithm is inspired by this [explanatory video](https://www.youtube.com/watch?v=P2TcQ3h0ipQ).



### Things I want to implement in the future (in descending order):
* Delay AI move, so it seems smoother
* Make CSS responsive for mobile devices
* Make it possible that the AI starts a round
* Improve AI so that it chooses the strategy to win with the least moves possible
* Have an option for the AIs to play against each other

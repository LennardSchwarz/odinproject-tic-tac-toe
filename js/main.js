const gameLogic = (() => {
  const gameScore = {
    X: 0,
    O: 0,
  };

  let currentPlayer = 'X';

  //module functions
  const init = () => {
    gameLogic.setBoard();

    //Add EventListeners on tiles
    const tiles = document.querySelectorAll('.tile');
    tiles.forEach(tile => {
      tile.addEventListener('click', handleNextTurn);
    });

    displayController.displayPlayerScore();

    //remove winner animation
    const playerScores = document.querySelectorAll('.score');
    playerScores.forEach(score => {
      score.classList.remove('winner');
    });

    //remove draw animation
    playerScores.forEach(score => {
      score.classList.remove('draw');
    });

  };
  const setBoard = () => {
    gameBoard.fill(''); //empty board

    currentPlayer = 'X'; //reset so X always starts

    const tiles = document.querySelectorAll('.tile');
    tiles.forEach(tile => {
      tile.textContent = gameBoard[tile.id];
    });
  };


  function handleNextTurn(e) {
    //check if tile is empty
    const index = e.target.id;
    if (gameBoard[index] !== '') return;


    switch (currentPlayer) {
      case "X":
        handleBoard(e, "X");
        if (AI.checkIfAI('O')) {
          AI.playNextBestMove(gameBoard, 'O');
          break;
        }
        currentPlayer = "O"; //switch player if human player
        break;
      case "O":
        handleBoard(e, "O");
        currentPlayer = "X"; //switch player
        break;
    }
  }

  function handleBoard(e, playerSymbol) {
    let index;

    if (e.index || e.index === 0) {//check if ai input. WHY does if clause not catch for e.index = 0????
      index = e.index
    } else if (e.target) { //check if human input
      index = e.target.id;
    }

    gameBoard[index] = playerSymbol;

    displayController.updateGameTiles();

    //check for win
    let gameWon = checkWin(gameBoard, playerSymbol);
    if (gameWon) handleWin(gameWon);

    //check for draw
    let draw = checkDraw(gameBoard);
    if (draw) handleDraw();
  }


function checkWin(board, player) {
  const winCombos = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [6, 4, 2]
  ];


  let plays = board.reduce((a, e, i) =>
    (e === player) ? a.concat(i) : a, []);
  let gameWon = null;
  for (let [index, win] of winCombos.entries()) {
    if (win.every(elem => plays.indexOf(elem) > -1)) {
      gameWon = {index: index, player: player};
      break;
    }
  }
  return gameWon;
}

function handleWin(gameWon) {
  const winner = gameWon.player;

  gameScore[winner]++;

  //update Scoreboard
  displayController.displayPlayerScore();

  removeTileEventListeners();

  //animate winner
  displayController.animateWinner(winner);

}

function checkDraw(board) {
  return !board.includes('');
}


function handleDraw() {
  removeTileEventListeners();

  //update Scoreboard
  displayController.displayPlayerScore();

  //animate draw
  displayController.animateDraw();

}

function removeTileEventListeners() {
  const tiles = document.querySelectorAll('.tile');
  tiles.forEach(tile => {
    tile.removeEventListener('click', handleNextTurn);
  });

}

  return { init, setBoard, handleBoard, gameScore, checkWin, checkDraw, currentPlayer
  };
})();

const AI = (() => {
  //check if AI should play TODO: make this logic simpler. Just check if input fields are empty and if yes, play next best move AI
  function checkIfAI(playerSymbol) {
    //make array of input
    const playerInputs = [];
    playerNameInputs.forEach(playerNameInput => {
      if (playerNameInput.value !== '') {
        playerInputs.push(playerNameInput.value);
      }
    });

   /* if (playerInputs.length === 0) {

      return true;
    } else {
      return false;
    }*/

    return playerInputs.length === 0

  }

  function playNextBestMove(board, playerSymbol) {
    const bestMove = miniMax(gameBoard, playerSymbol);

    //play ai turn after a timeout because seems smoother
    gameLogic.handleBoard(bestMove, playerSymbol);


  }

  const miniMax = (board, player) => {
    let huPlayer = 'X'; //TODO: should not be hardcoded
    let aiPlayer = 'O';
    const availableSquares = calcAvailableSquares(board);

      // terminal conditions
      if (gameLogic.checkWin(board, aiPlayer)) return { score: 10 };
      if (gameLogic.checkWin(board, huPlayer)) return { score: -10 };
      if (gameLogic.checkDraw(board)) return { score: 0 };

      // save all the possible moves from current board in an array
      let moves = [];

      // loop over available moves
      for (let i = 0; i < availableSquares.length; i++) {

        const tempBoard = [...board];

        let move = {};

        // save the squares location (index in array)
        move.index = availableSquares[i];

        // fill in square with current player
        tempBoard[availableSquares[i]] = player;

        // find score of move -> recursion entry point
        if (player === aiPlayer) {
          const result = miniMax(tempBoard, huPlayer);
          move.score = result.score;
        }  if (player === huPlayer) {
          const result = miniMax(tempBoard, aiPlayer);
          move.score = result.score;
        }

        // save results of move to array
        moves.push(move);
      }

      let bestMove;

      // if player is ai - bestScore is set to -100 - else if human 100
      let bestScore = player === aiPlayer ? -100 : 100;

      // find best move from possible moves
      moves.forEach(move => {
        if (player === aiPlayer) {
          if (move.score > bestScore) {
            bestScore = move.score;
            bestMove = move;
          }
        } else {
          if (move.score < bestScore) {
            bestScore = move.score;
            bestMove = move;
          }
        }
      }); return bestMove;
    };

  function calcAvailableSquares(board) {
    //return array of indices of available tiles
    const availableSquares = [];
     board.forEach((s, index) => {
      if (s === '') {
        availableSquares.push(index);
      }
    });

    return availableSquares;
  }



    return { checkIfAI, playNextBestMove };
  }
)();

/*const playerFactory = (name, playerSymbol) => {
  const getName = () => name;
  const getPlayerSymbol = () => playerSymbol;
  function setMarker (e) {
    e.target.textContent = playerSymbol;
  }
  return {
    getName, getPlayerSymbol, setMarker
  };
};*/

const gameBoard = ['','','','','','','','',''];
 // ['O', 'X', 'O', '', 'O', 'X', '', 'X', '']; //3x3 grid array




const displayController = (() => {
  function displayPlayerScore() {
    //grab divs
    const playerX = document.querySelector('#score-player-X');
    const playerO = document.querySelector('#score-player-O');

    //player names
    const nameX = document.querySelector('#player-x-name').value || 'X';
    const nameO = document.querySelector('#player-o-name').value || 'O';

    //player scores
    const scoreX = gameLogic.gameScore.X;
    const scoreO = gameLogic.gameScore.O;

    //update text of both divs
    playerX.textContent = `Wins ${nameX}: ${scoreX}`;
    playerO.textContent = `Wins ${nameO}: ${scoreO}`;
  }

  function animateWinner (winner) {
    const winnerDiv = document.querySelector(`#score-player-${winner}`);
    winnerDiv.classList.add('winner');
  }

  function animateDraw() {
    const scores = document.querySelectorAll('.score');
    scores.forEach(score => {
      score.classList.add('draw');
    })
  }

  function updateGameTiles() {
    const tiles = document.querySelectorAll('.tile');
    tiles.forEach(tile => {
      tile.textContent = gameBoard[tile.id];
    });
  }

  return {
    displayPlayerScore, animateWinner, animateDraw, updateGameTiles
  };

})();



//global eventListeners
//setup
window.addEventListener('DOMContentLoaded', () => {
  gameLogic.init();
});

//new Game
const newGameButton = document.querySelector('#new-game-button');
newGameButton.addEventListener('click', gameLogic.init);

//player name input
const playerNameInputs = document.querySelectorAll('input');
playerNameInputs.forEach(playerNameInput => {
  playerNameInput.addEventListener('keyup', displayController.displayPlayerScore);
});
